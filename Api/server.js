'use strict'

var mongoose = require('mongoose');

var port = process.env.PORT || 8080;
var database = process.env.DATABASE || "restaurante";

var app = require('./app');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/' + database, { useNewUrlParser: true })
    .then(() => {
        console.log("The connection to the " + database + "database has been correctly made")
        // CREAR EL SERVIDOR WEB CON NODEJS
        app.listen(port, () => {
            console.log("Server is running in http://localhost:" + port);
        });
    })
    .catch(err => console.log(err));
console.log('Magic happens on port ' + port);


