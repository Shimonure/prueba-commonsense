'use strict'

var express    = require('express');        // call express
var cors       = require('cors');
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
app.get('/', function (req, res) {
    res.json({ message: 'Api de prueba' });
});

// all of our routes will be prefixed with /api
require('./app/routes/platillo.route.js')(app);
require('./app/routes/comanda.route.js')(app);

module.exports = app;