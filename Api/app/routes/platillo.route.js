module.exports = (app) => {
    const platillos = require('../controllers/platillo.controller');
    // Create a new Note
    app.post('/platillos', platillos.create);

    // Retrieve all platillos
    app.get('/platillos', platillos.findAll);

    // Retrieve a single Note with platilloId
    app.get('/platillos/:platilloId', platillos.findOne);

    // Update a Note with platilloId
    app.put('/platillos/:platilloId', platillos.update);

    // Delete a Note with platilloId
    app.delete('/platillos/:platilloId', platillos.delete);
}