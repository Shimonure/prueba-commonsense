module.exports = (app) => {
    const comandas = require('../controllers/comanda.controller');
    // Create a new Note
    app.post('/comandas', comandas.create);

    // Retrieve all comandas
    app.get('/comandas', comandas.findAll);

    // Retrieve a single Note with comandaId
    app.get('/comandas/:comandaId', comandas.findOne);

    // Update a Note with comandaId
    app.put('/comandas/:comandaId', comandas.update);

    // Delete a Note with comandaId
    app.delete('/comandas/:comandaId', comandas.delete);
}