var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var PlatilloSchema = require('mongoose').model('Platillo').schema

var ComandaSchema = new Schema(
    {
        mesa: String,
        descripcion: String,
        platillos: [],
        created_at: { type: Date, default: Date.now },
        updated_at: { type: Date, default: Date.now },
    },
    {
        versionKey: false
    }
);

module.exports = mongoose.model('Comanda', ComandaSchema);