var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PlatilloSchema = new Schema(
    {
        nombre: String,
        descripcion: String
    },
    {
        versionKey: false
    }
);

module.exports = mongoose.model('Platillo', PlatilloSchema);