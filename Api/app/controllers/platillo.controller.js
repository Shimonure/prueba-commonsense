const Platillo = require('../models/platillo.model.js');

// Create and Save a new Platillo
exports.create = (req, res) => {
    // Validate request
    if (!req.body.nombre) {
        return res.status(400).send({
            message: "Platillo name can not be empty"
        });
    }

    // Create a Platillo
    const platillo = new Platillo({
        nombre: req.body.nombre,
        descripcion: req.body.descripcion
    });

    // Save Platillo in the database
    platillo.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Platillo."
            });
        });
};

// Retrieve and return all platillos from the database.
exports.findAll = (req, res) => {
    Platillo.find()
        .then(platillos => {
            res.send(platillos);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving platillos."
            });
        });
};

// Find a single platillo with a platilloId
exports.findOne = (req, res) => {
    Platillo.findById(req.params.platilloId)
        .then(platillo => {
            if (!platillo) {
                return res.status(404).send({
                    message: "Platillo not found with id " + req.params.platilloId
                });
            }
            res.send(platillo);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Platillo not found with id " + req.params.platilloId
                });
            }
            return res.status(500).send({
                message: "Error retrieving platillo with id " + req.params.platilloId
            });
        });
};

// Update a platillo identified by the platilloId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body.nombre) {
        return res.status(400).send({
            message: "Platillo name can not be empty"
        });
    }

    // Find platillo and update it with the request body
    Platillo.findByIdAndUpdate(req.params.platilloId, {
        nombre: req.body.nombre,
        descripcion: req.body.descripcion
    }, { new: true })
        .then(platillo => {
            if (!platillo) {
                return res.status(404).send({
                    message: "Platillo not found with id " + req.params.platilloId
                });
            }
            res.send(platillo);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Platillo not found with id " + req.params.platilloId
                });
            }
            return res.status(500).send({
                message: "Error updating platillo with id " + req.params.platilloId
            });
        });
};

// Delete a platillo with the specified platilloId in the request
exports.delete = (req, res) => {
    Platillo.findByIdAndRemove(req.params.platilloId)
        .then(platillo => {
            if (!platillo) {
                return res.status(404).send({
                    message: "Platillo not found with id " + req.params.platilloId
                });
            }
            res.send({ message: "Platillo deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "Platillo not found with id " + req.params.platilloId
                });
            }
            return res.status(500).send({
                message: "Could not delete platillo with id " + req.params.platilloId
            });
        });
};