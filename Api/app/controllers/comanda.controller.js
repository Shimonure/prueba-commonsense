const Comanda = require('../models/comanda.model.js');

// Create and Save a new Comanda
exports.create = (req, res) => {
    // Validate request
    if (!req.body.mesa || !req.body.platillos) {
        return res.status(400).send({
            message: "Comanda content can not be empty"
        });
    }

    // Create a Comanda
    const comanda = new Comanda({
        mesa: req.body.mesa,
        descripcion: req.body.descripcion,
        platillos: req.body.platillos
    });

    // Save Comanda in the database
    comanda.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Comanda."
            });
        });
};

// Retrieve and return all comandas from the database.
exports.findAll = (req, res) => {
    Comanda.find()
        .then(comandas => {
            res.send(comandas);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving comandas."
            });
        });
};

// Find a single comanda with a comandaId
exports.findOne = (req, res) => {
    Comanda.findById(req.params.comandaId)
        .then(comanda => {
            if (!comanda) {
                return res.status(404).send({
                    message: "Comanda not found with id " + req.params.comandaId
                });
            }
            res.send(comanda);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Comanda not found with id " + req.params.comandaId
                });
            }
            return res.status(500).send({
                message: "Error retrieving comanda with id " + req.params.comandaId
            });
        });
};

// Update a comanda identified by the comandaId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body.mesa || !req.body.platillos) {
        return res.status(400).send({
            message: "Comanda content can not be empty"
        });
    }

    // Find comanda and update it with the request body
    Comanda.findByIdAndUpdate(req.params.comandaId, {
        title: req.body.mesa,
        descripcion: req.body.descripcion,
        platillos: req.body.platillos,
    }, { new: true })
        .then(comanda => {
            if (!comanda) {
                return res.status(404).send({
                    message: "Comanda not found with id " + req.params.comandaId
                });
            }
            res.send(comanda);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Comanda not found with id " + req.params.comandaId
                });
            }
            return res.status(500).send({
                message: "Error updating comanda with id " + req.params.comandaId
            });
        });
};

// Delete a comanda with the specified comandaId in the request
exports.delete = (req, res) => {
    Comanda.findByIdAndRemove(req.params.comandaId)
        .then(comanda => {
            if (!comanda) {
                return res.status(404).send({
                    message: "Comanda not found with id " + req.params.comandaId
                });
            }
            res.send({ message: "Comanda deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "Comanda not found with id " + req.params.comandaId
                });
            }
            return res.status(500).send({
                message: "Could not delete comanda with id " + req.params.comandaId
            });
        });
};