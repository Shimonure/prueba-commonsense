import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Site from './layout/Site'
import Header from './layout/Header'
import Content from './layout/Content'
import Footer from './layout/Footer'


const Layout = ({ children }) => (
    <Site>
        <Helmet
            title="Prueba técnica Full Stack"
            meta={[
                { name: 'description', content: 'Prueba técnica para Common Sense' },
                { name: 'keywords', content: 'test, react, express, node' },
            ]}
            script={[
                
            ]}
            link={[
                
            ]}
        />
        <Header />
        <Content>
            {/* {children()} */}
        </Content>
        <Footer />
    </Site>
)

Layout.propTypes = {
    children: PropTypes.func,
}

export default Layout