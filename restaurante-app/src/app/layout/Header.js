import { Link } from 'react-router-dom';
import React from 'react'
import $ from 'jquery';

class Header extends React.Component {

    toggleNav = () => {
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");
    }
    render() {
        return (
            <nav className="navbar is-primary">
                <div className="container">
                    <div className="navbar-brand">
                        <a className="navbar-item" >
                            Restaurante
                        </a>
                        <span className="navbar-burger burger" onClick={this.toggleNav} data-target="navMenu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </div>
                    <div id="navMenu" className="navbar-menu">
                        <div className="navbar-end">
                            <Link to="/" className="navbar-item">Home</Link>
                            <Link to="/comandas" className="navbar-item">Comandas</Link>
                            <Link to="/platillos" className="navbar-item">Platillos</Link>
                            <Link to="/about" className="navbar-item">About</Link>
                        </div>
                    </div>
                </div>
            </nav>
        )
    }
}

export default Header