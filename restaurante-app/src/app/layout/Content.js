import React from 'react'
import { Route } from 'react-router-dom';
import Home from '../../components/Home';
import About from '../../components/About';
import Platillos from '../../components/Platillos';
import Comandas from '../../components/Comandas';

class Header extends React.Component {	
	render() {
		return (
			<div>
			<Route exact path="/" component={Home} />
			<Route path="/about" component={About} />
			<Route path="/platillos" component={Platillos} />
			<Route path="/comandas" component={Comandas} />
			</div>

		)
	}
}

export default Header