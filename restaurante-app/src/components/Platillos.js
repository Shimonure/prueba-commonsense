import React from 'react';
import axios from 'axios';



class Platillos extends React.Component {

    state = {
        platillos: [],
        nombre: "",
        descripcion: ""

    }
    handleInputChange = (e) => {
        const target = e.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    getPlatillos = () => {
        axios.get('http://localhost:8080/platillos')
            .then(res => {
                const platillos = res.data;
                this.setState({ platillos });
            })
    }
    agregarPlatillo = () => {
        const platillo = {
            nombre: this.state.nombre,
            descripcion: this.state.descripcion,
        };
        console.log(platillo);
        axios.post('http://localhost:8080/platillos', platillo)
            .then(res => {
                this.getPlatillos();
                this.setState({ nombre: "" });
                this.setState({ descripcion: "" });
                console.log(res);
                console.log(res.data);

            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response.data);
                    console.log(error.response.status);
                    //console.log(error.response.headers);
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
            })


    }
    deletePlatillo = (platilloId) => {
        axios.delete('http://localhost:8080/platillos/' + platilloId)
            .then(res => {
                this.getPlatillos();
                console.log(res);
                console.log(res.data);
            })
    }
    componentDidMount() {
        this.getPlatillos();
    }

    render() {
        return (
            <div className="container">
                <section className="section">
                    <div className="container">
                        <h1 className="title">Platillos</h1>
                        <h2 className="subtitle">Sección para administración de platillos</h2>
                    </div>
                    <br />
                    <form>
                        <div className="field is-grouped">
                            <p className="control is-expanded">
                                <input className="input" type="text" name="nombre" placeholder="Nombre" value={this.state.nombre} onChange={this.handleInputChange} />
                            </p>
                            <p className="control is-expanded">
                                <input className="input" type="text" name="descripcion" placeholder="Descripción" value={this.state.descripcion} onChange={this.handleInputChange} />
                            </p>
                            <p className="control is-expanded">
                                <a className="button is-success" onClick={(e) => this.agregarPlatillo(e)}><i className="fa fa-plus" aria-hidden="true"></i></a>
                            </p>
                        </div>
                    </form>
                    <br />
                    <table className="table">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Nombre</td>
                                <td>Descripción</td>
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.platillos.map(platillo =>
                                (<tr key={platillo._id}>
                                    <td>{platillo._id}</td>
                                    <td>{platillo.nombre}</td>
                                    <td>{platillo.descripcion}</td>
                                    <td>
                                        <a className="button is-danger" onClick={(e) => this.deletePlatillo(platillo._id, e)}><i className="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </section>
            </div>
        );
    }
}

export default Platillos;