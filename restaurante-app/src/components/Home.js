import { Link } from 'react-router-dom';
import React from 'react';
import axios from 'axios';

class Home extends React.Component {
  state = {
		comandas: []
	}
	getComandas = () => {
		axios.get('http://localhost:8080/comandas')
				.then(res => {
						const comandas = res.data;
						this.setState({ comandas });
				})
  }
  componentDidMount() {
    this.getComandas();
  }
  render(){
    return (
      <div className="container ">
        <section className="section">
        <div className="columns is-multiline">
        <div className="column is-12">
          <div className="columns">
            <div className="column is-6">
              <h1 className="title p-2">Ordenes atendidas</h1>
            </div>
            <div className="column is-6">
              <Link to="/comandas" className="button is-success">Nueva Orden</Link>
            </div>
          </div>  
        </div>
        <div className="column is-12">
          <table className="table">
            <thead>
              <tr>                
                <td><abbr title="Mesa">Mesa</abbr></td>
                <td><abbr title="Descripción">Descripción</abbr></td>
                <td><abbr title="Ordenaron">Platillos</abbr></td>
                <td><abbr title=""></abbr></td>
              </tr>
            </thead>
            <tbody>
            {
              
              this.state.comandas.map(comanda =>
                (
                  <tr key={comanda._id}>                
                    <th>{comanda.mesa}</th>
                    <td>{comanda.descripcion}</td>
                    <td><ul>{comanda.platillos.map((platillo,i) =>(<li key={i}>{platillo.nombre}</li>))}</ul></td>
                  </tr>
                    
                )
              )
            }
            </tbody>  
          </table>
        </div>
        </div>
      </section>
    </div>
    )
  }
}

export default Home;