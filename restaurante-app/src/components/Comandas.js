import React from 'react';
import axios from 'axios';

class Comandas extends React.Component {
    state = {
        listaPlatillos: [],
        mesa: "",
        descripcion: "",
        platillosInComanda: [],
    }
    handleInputChange = (e) => {
        const target = e.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }
    getPlatillos = () => {
        axios.get('http://localhost:8080/platillos')
            .then(res => {
                const listaPlatillos = res.data;
                this.setState({ listaPlatillos });
            })
    }
    addFruit = (fruit) => {
        var timestamp = (new Date()).getTime();
        // update the state object
        this.state.fruits['fruit-' + timestamp ] = fruit;
        // set the state
        this.setState({ fruits : this.state.fruits });
    }
    agregarPlatilloToComanda = (platillo, event) => {
        var timestamp = (new Date()).getTime();
        this.state.platillosInComanda['platillo-' + timestamp ] = platillo;
        this.setState({
            platillosInComanda: this.state.platillosInComanda
        });
        //console.log(Object.keys(this.state.platillosInComanda))
        //console.log(this.state.platillosInComanda);

    }
    eliminarPlatilloEnComanda = (key,event) =>{
        var array = this.state.platillosInComanda;
        delete array[key];
        //console.log (array);
        this.setState({platillosInComanda: array});
    }
    guardarComanda(){
        //console.log("N",this.state.mesa);
        //console.log(this.state.platillosInComanda);
        var a = Object.values(this.state.platillosInComanda);
        var idsPlatillos = [];
        a.forEach(function(platillo) {
            idsPlatillos.push(
                {
                    _id: platillo._id,
                    nombre: platillo.nombre,
                    descripcion: platillo.nombre
                }
            );
        });

        const comanda = {
            mesa: this.state.mesa,
            descripcion: this.state.descripcion,
            platillos: idsPlatillos
        };
        //console.log(comanda);
        
        axios.post('http://localhost:8080/comandas', comanda)
            .then(res => {
                //console.log(res.data);
                this.setState({platillosInComanda: []});
                this.setState({mesa: ""});
                this.setState({descripcion: ""});
            
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response.data);
                    console.log(error.response.status);
                    //console.log(error.response.headers);
                    
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
            })
    }
    componentDidMount() {
        this.getPlatillos();
    }
    render() {
        return (
            <div className="container">
                <section className="section">

                    <h3 id="styles" className="title is-4 is-spaced bd-anchor-title">
                        <span className="bd-anchor-name">
                            Platillos, presiona el platillo para agregar a la comanda
                        </span>
                    </h3>

                    <div className="columns is-multiline">
                        <div className="column is-6">
                            <form>
                                <div className="field">
                                    <p className="control is-expanded">
                                        <input className="input" type="number" name="mesa" placeholder="Mesa" value={this.state.mesa} onChange={this.handleInputChange} />
                                    </p>
                                </div>
                                <div className="field">
                                    <p className="control is-expanded">
                                        <input className="input" type="textarea" name="descripcion" placeholder="Descripcion" value={this.state.descripcion} onChange={this.handleInputChange} />
                                    </p>
                                </div>
                                <div className="field">
                                    <p className="control is-expanded">
                                        <a className="button is-success" onClick={(e) => this.guardarComanda(e)}>Guardar</a>
                                    </p>
                                </div>
                            </form>
                        </div>
                        <div className="column is-6">
                            <div className="buttons">
                                {this.state.listaPlatillos.map(platillo =>
                                    (
                                        <a key={platillo._id}
                                            className="button is-info is-outlined tooltip"
                                            data-tooltip={platillo.descripcion}
                                            onClick={(e) => this.agregarPlatilloToComanda(platillo, e)}
                                        >
                                            {platillo.nombre}
                                        </a>
                                    ))
                                }
                            </div>
                        </div>
                        <div className="column is-12">
                            <div className="columns is-multiline">
                            {
                                Object.keys(this.state.platillosInComanda).map((keyValue,i) => (
                                    <div key={i} className="column is-3">
                                        <div className="notification is-primary">
                                            <button key={'del-' + keyValue} className="delete" onClick={(e) => this.eliminarPlatilloEnComanda(keyValue, e)}></button>
                                            {this.state.platillosInComanda[keyValue].nombre}
                                        </div>
                                    </div>
                                ))
                            
                            }
                            </div>
                        </div>
                    </div>
                </section>
                
                    
            </div >
        )
    }
}
export default Comandas;